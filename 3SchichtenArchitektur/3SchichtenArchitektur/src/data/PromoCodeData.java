package data;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class PromoCodeData implements IPromoCodeData {

	private List<String> promocodes;
	
	public PromoCodeData(){
		this.promocodes = new LinkedList<String>();
	}
	
	@Override
	public boolean savePromoCode(String code) {
		FileWriter fwriter;
		File promocodes = new File("Promocodes.txt");
		
		try {
			fwriter = new FileWriter(promocodes, true);
			fwriter.write(code+ "\n");
			
			fwriter.flush();
			fwriter.close();
		} catch (IOException e) {
			//TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return this.promocodes.add(code);
	}

	@Override
	public boolean isPromoCode(String code) {
		return this.promocodes.contains(code);
	}

}
