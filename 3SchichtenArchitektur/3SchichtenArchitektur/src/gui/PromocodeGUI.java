package gui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import logic.IPromoCodeLogic;
import javax.swing.ImageIcon;
import java.awt.Color;

public class PromocodeGUI extends JFrame {

	private static final long serialVersionUID = 2303911575322895620L;
	private JPanel contentPane;
	private JLabel lblPromocode;

	/**
	 * Create the frame.
	 */
	public PromocodeGUI(IPromoCodeLogic logic) {
		setTitle("PromotionCodeGenerator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewPromoCode = new JButton("Generate code");
		btnNewPromoCode.setBackground(Color.WHITE);
		btnNewPromoCode.setFont(new Font("Microsoft YaHei UI", Font.BOLD, 11));
		btnNewPromoCode.setBounds(149, 127, 125, 23);
		btnNewPromoCode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String code = logic.getNewPromoCode();
				lblPromocode.setText(code);
			}
		});
		contentPane.add(btnNewPromoCode);
		
		JLabel lblBeschriftungPromocode = new JLabel("Promotioncode f�r 2-fach IT$ in einer Stunde");
		lblBeschriftungPromocode.setForeground(Color.WHITE);
		lblBeschriftungPromocode.setFont(new Font("Microsoft YaHei UI", Font.BOLD, 14));
		lblBeschriftungPromocode.setBounds(63, 11, 330, 28);
		contentPane.add(lblBeschriftungPromocode);
		
		lblPromocode = new JLabel("");
		lblPromocode.setForeground(Color.WHITE);
		lblPromocode.setBounds(0, 0, 434, 161);
		lblPromocode.setHorizontalAlignment(SwingConstants.CENTER);
		lblPromocode.setFont(new Font("Microsoft YaHei UI", Font.BOLD, 18));
		contentPane.add(lblPromocode);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(PromocodeGUI.class.getResource("/images/unknown.png")));
		lblNewLabel.setBounds(0, 0, 434, 161);
		contentPane.add(lblNewLabel);
		this.setVisible(true);
	}
}
